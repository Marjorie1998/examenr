package conforme.marjorie.pm4a.examen_recuperacion;

public class MODELO {
    private  String campo1,campo2, campo3, campo4,foto;
    private int campo6;

    public MODELO(String campo1, String campo2, String campo3, String campo4,String foto, int campo6) {
        this.campo1 = campo1;
        this.campo2 = campo2;
        this.campo3 = campo3;
        this.campo4 = campo4;
        this.campo6 = campo6;
        this.foto=foto;
    }


    public String getCampo1() {
        return campo1;
    }

    public void setCampo1(String campo1) {
        this.campo1 = campo1;
    }

    public String getCampo2() {
        return campo2;
    }

    public void setCampo2(String campo2) {
        this.campo2 = campo2;
    }

    public String getCampo3() {
        return campo3;
    }

    public void setCampo3(String campo3) {
        this.campo3 = campo3;
    }

    public String getCampo4() {
        return campo4;
    }

    public void setCampo4(String campo4) {
        this.campo4 = campo4;
    }

    public int getCampo6() {
        return campo6;
    }

    public void setCampo6(int campo6) {
        this.campo6 = campo6;


    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
