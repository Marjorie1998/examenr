package conforme.marjorie.pm4a.examen_recuperacion;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.os.Build.VERSION_CODES.P;

public class ADAPTADOR extends RecyclerView.Adapter<ADAPTADOR.ViewHolder> {

    ArrayList<MODELO>modelosArrayList;


    public ADAPTADOR(ArrayList<MODELO> modelosArrayList) {
        this.modelosArrayList = modelosArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        return new ViewHolder(LayoutInflater.from(  parent.getContext()).inflate(R.layout.item_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       MODELO modelo = modelosArrayList.get(position);
        holder.campo1.setText(modelo.getCampo1());
        holder.campo2.setText(modelo.getCampo2());
        Picasso.get().load(modelosArrayList.get(position).getFoto()).into(holder.foto);




    }

    @Override
    public int getItemCount() {
        return modelosArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView campo1,campo2;
        ImageView foto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            campo1=(TextView)itemView.findViewById(R.id.campo1);
            campo2=(TextView)itemView.findViewById(R.id.campo2);
            foto=(ImageView)itemView.findViewById(R.id.imagen);
        }
    }
}
